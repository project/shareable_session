<?php

/**
 * @file
 * Helper functions.
 */

/**
 * Show shareable session link block content.
 *
 * If NULL will generate the current page URL.
 */
function shareable_session_generate_link($page_path = NULL, $user_id = NULL) {

  // Set default value of page path.
  if ($page_path == NULL) {
    // Getting current page path.
    // If the menu is currently being rebuilt, menu_get_item will cause an
    // infinite loop. That's why we will check the cache directly.
    // (snippet from event_log module).
    $router_items = &drupal_static('menu_get_item');
    if (!empty($router_items[$_GET['q']])) {
      $item = $router_items[$_GET['q']];
    }

    if (!empty($item)) {
      $page_path = $item['path'];
    }
  }

  // Set user uid.
  if ($user_id == NULL) {
    // Getting current user id.
    global $user;
    $user_id = $user->uid;
  }

  $token = shareable_session_generate_link_token();
  return url($page_path, array(
    'absolute' => TRUE,
    'query' => array(
      'uid' => 0,
      'path' => $page_path,
      'token' => $token,
    ),
  ));
}

/**
 * Generate link token.
 */
function shareable_session_generate_link_token() {
  $hash = time() . microtime();
  return md5($hash);
}
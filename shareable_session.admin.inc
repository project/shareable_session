<?php

/**
 * @file
 * Admin page callbacks.
 */

/**
 * Module settings form.
 */
function shareable_session_settings_form($form, &$form_state) {
  $form['shareable_session_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Shareable session'),
    '#default_value' => variable_get('shareable_session_status', FALSE),
  );

  return system_settings_form($form);
}

Sharable session Module to save a user session and share the session.
This will define a block where you can assign it to any region. And this block will have a direct link to this pag
Allow sharing user session using an URL with a destination. Useful when testers want to report a bug and they will include this direct URL in the issue to allow developers to login directly and see the bug. Also, this will increase the testers awareness that the URL is important and very helpful.

Roadmap:
[] Create a block to hold the shareable session link.
[] Having a public page where users can view all shared sessions.
[] Add a custom token in URL for the public page to allow the user to view it anonymously when they have the token inserted in the URL. And any viewing of this page without that token it will be denied.
[] Shareable session will be only for a specific custom user role not for the authenticated role. (The module will create a new role called "shareable session" for you. You have to assign the users to it).
[] Allow changing the user role to any other role.

<?php

/**
 * @file
 * Define blocks for shareable session module.
 */

/**
 * Implements hook_block_info().
 *
 * Define block to show shareable session link.
 */
function shareable_session_block_info() {
  $blocks = array();
  // Block to show shareable session link.
  $blocks['shareable_session_link'] = array(
    'info' => t('Shareable session link'),
    'status' => FALSE,
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function shareable_session_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'shareable_session_link':
      $block['subject'] = t('Shareable session link');
      $block['content'] = shareable_session_link_block_content();
      break;
  }
  return $block;
}

/**
 * Show shareable session link block content.
 *
 * If this function return FALSE the block will be hidden.
 */
function shareable_session_link_block_content() {
  // Check if the shareable session status is On.
  if (1 == variable_get('shareable_session_status', 0)) {
    // Create shareable session link.
    $shareable_session_link = shareable_session_generate_link();
    $result = array(
      '#markup' => '<input type="textfield" style="width:100%;" onClick="this.select();" value="' . $shareable_session_link . '">',
    );
    return $result;
  }
  else {
    return FALSE;
  }
}